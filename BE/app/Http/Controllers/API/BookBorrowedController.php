<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\BorrowedBookRequest;
use App\Http\Controllers\Controller;
use App\Models\borrowed_books;

class BookBorrowedController extends Controller
{
    public function index()
    {
        $borrowed = borrowed_books::all();
        return response()->json($borrowed, 200);
    }

    public function store(BorrowedBookRequest $request)
    {
        $borrowed = borrowed_books::create($request->all());
        return response()->json($borrowed, 201);
    }

    public function update(BorrowedBookRequest $request, $id)
    {
        $borrowed = borrowed_books::where('id', $id)->update($request->all(), $id);
        return response()->json($borrowed, 200);
    }

    public function destroy($id)
    {
        $borrowed = borrowed_books::find($id);
        $borrowed->delete($id);

        return response('Borrowed Book Deleted');
    }
}
