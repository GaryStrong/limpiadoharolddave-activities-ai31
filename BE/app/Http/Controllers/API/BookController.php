<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BookRequest;
use App\Models\books;

class BookController extends Controller
{
    public function index()
    {
        $books = books::all();

        return response()->json($books, 200);
    }

    public function store(BookRequest $request)
    {
        $books = books::create($request->all());
        return response()->json($books, 201);
    }

    public function update(BookRequest $request, $id)
    {
        $books = books::where('id', $id)->update($request->all(), $id);
        return response()->json($books, 200);
    }

    public function destroy($id)
    {
        $books = books::find($id);
        $books->delete();

        return response('Book Deleted');
    }
}
