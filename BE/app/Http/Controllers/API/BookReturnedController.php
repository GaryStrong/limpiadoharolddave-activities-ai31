<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\ReturnedBookRequest;
use App\Http\Controllers\Controller;
use App\Models\returned_books;

class BookReturnedController extends Controller
{
    public function index()
    {
        $returned = returned_books::all();
        return response()->json($returned, 200);
    }

    public function store(ReturnedBookRequest $request)
    {
        $returned = returned_books::create($request->all());
        return response()->json($returned, 201);
    }

    public function update(ReturnedBookRequest $request, $id)
    {
        $returned = returned_books::where('id', $id)->update($request->all(), $id);
        return response()->json($returned, 200);
    }

    public function destroy($id)
    {
        $returned = returned_books::find($id);
        $returned->delete($id);

        return response('Returned Book Deleted');
    }
}
