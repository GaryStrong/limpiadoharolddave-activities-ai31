<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Http\Controllers\Controller;
use App\Models\categories;

class CategoriesController extends Controller
{
    public function index()
    {
        $category = categories::all();
        return response()->json($category, 200);
    }

    public function store(CategoryRequest $request)
    {
        $category = categories::create($request->all());
        return response()->json($category, 201);
    }

    public function update(CategoryRequest $request, $id)
    {
        $category = categories::where('id', $id)->update($request->all(), $id);
        return response()->json($category, 200);
    }

    public function destroy($id)
    {
        $category = categories::find($id);
        $category->delete($id);

        return response('Category Deleted');
    }
}
