<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\PatronRequest;
use App\Http\Controllers\Controller;
use App\Models\patrons;

class PatronController extends Controller
{
    public function index()
    {
        $patrons = patrons::all();
        return response()->json($patrons, 200);
    }
   
    public function store(PatronRequest $request)
    {
        $patron = patrons::create($request->all());
        return response()->json($patron, 201);
    }

    public function update(PatronRequest $request, $id)
    {
        $patron = patrons::where('id', $id)->update($request->all(), $id);
        return response()->json($patron, 200);
    }

    public function destroy($id)
    {
        $patron = patrons::find($id);
        $patron->delete($id);

        return response('Patron Deleted');
    }
}
