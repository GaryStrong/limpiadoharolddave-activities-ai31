<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class BookRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'author' =>  'required',
            'copies' =>  'required|integer|min:1',
            'category_id' => 'required|exists:categories,id'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A book name is required',
            'author.required' => 'An author is required',
            'copies.required' => 'Copies of a book is required',
            'copies.integer' => 'Number of copies must be an integer',
            'category_id.required' => 'This books should have a category',
            'category_id.exists' => 'ID not found!'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
