<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\books;

class BorrowedBookRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $book = books::find(request()->get('book_id'));
        $copies = $book->copies;

        return [
            'patron_id' => 'required|exists:patrons,id',
            'copies' =>  "required|integer|lte: {$copies}|min:1",
            'book_id' =>  'required|exists:books,id'
        ];
    }

    public function messages()
    {
        return [
            'patron_id.required' => 'A patron is required',
            'patron_id.exists' => 'A borrowed book should have a patron',
            'copies.required' => 'Copies of borrowed book is required',
            'copies.integer' => 'Number of copies must be an integer',
            'copies.lte' => 'Number of copies borrowed exceeded',
            'book_id.required' => 'A book ID is required',
            'book_id.exists' => 'ID not found!'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
