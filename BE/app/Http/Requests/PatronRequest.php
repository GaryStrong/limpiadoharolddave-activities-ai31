<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class PatronRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'last_name' => 'required',
            'first_name' =>  'required',
            'middle_name' =>  'required',
            'email' => 'required|email',
        ];
    }

    public function messages()
    {
        return [
            'last_name.required' => 'A last name is required',
            'first_name.required' => 'A first name is required',
            'middle_name.required' => 'A middle name is required',
            'email.required' => 'An email is required',
            'email.email' => 'Email not recognized'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
