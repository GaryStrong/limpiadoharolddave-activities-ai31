<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\borrowed_books;

class ReturnedBookRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $borrowed = borrowed_books::find(request()->get('book_id'));
        $copies = $borrowed->copies;
        return [
            'book_id' => 'required|exists:borrowed_books,id',
            'copies' =>  "required|integer|lte: {$copies}|min:1",
            'patron_id' =>  'required|exists:borrowed_books,id'
        ];
    }

    public function messages()
    {
        return [
            'book_id.required' => 'A book ID is required',
            'book_id.exists' => 'ID not found!',
            'copies.required' => 'Copies of returned book is required',
            'copies.integer' => 'Number of copies must be an integer',
            'copies.lte' => 'Number of copies borrowed exceeded',
            'patron_id.required' => 'A patron is required',
            'patron_id.exists' => 'A borrowed book should have a patron'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
