<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class books extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function categoriesRelation(){
        return $this->hasOne(categories::class, 'category_id', 'id');
    }

    public function borrowedRelation(){
        return $this->hasMany(borrowed_books::class, 'book_id', 'id');
    }

    public function returnedRelation(){
        return $this->hasMany(returned_books::class, 'book_id', 'id');
    }
}
