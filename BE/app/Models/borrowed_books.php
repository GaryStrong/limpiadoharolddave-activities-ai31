<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class borrowed_books extends Model
{
    use HasFactory;

    protected $guarded = [];
    
    public function booksRelation(){
        return $this->belongsToMany(books::class, 'books', 'book_id', 'id');
    }

    public function patronRelation(){
        return $this->belongsToMany(patrons::class, 'patrons', 'patron_id', 'id');
    }
}
