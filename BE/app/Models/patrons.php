<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class patrons extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function borrowedRelation(){
        return $this->hasMany(borrowed_books::class, 'patron_id', 'id');
    }
    
    public function returnedRelation(){
        return $this->hasMany(returned_books::class, 'patron_id', 'id');
    }
}
