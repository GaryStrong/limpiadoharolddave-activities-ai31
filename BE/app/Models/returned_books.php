<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class returned_books extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function booksRelation(){
        return $this->belongsToMany(books::class, 'books', 'book_id', 'id');
    }

    public function patronsRelation(){
        return $this->belongsToMany(patrons::class, 'patrons', 'patron_id', 'id');
    }
}
