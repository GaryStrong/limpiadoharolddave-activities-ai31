<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReturnedBooksTable extends Migration
{
    public function up()
    {
        Schema::create('returned_books', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('books');
            $table->integer('copies');
            $table->integer('patron_id')->unsigned();
            $table->foreign('patron_id')->references('id')->on('patrons');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('returned_books');
    }
}
