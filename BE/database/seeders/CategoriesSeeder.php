<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            "Romance",
            "Fiction",
            "History",
            "Horror",
            "Educational"
        ];

        $date = Carbon::now();
        $createdDate = clone($date);

        foreach($categories as $catergory) {
            DB::table('categories')->insert(
                ['category' => $catergory,
                'created_at' => $createdDate,
                'updated_at' => $createdDate]
            );
        }
    }
}
