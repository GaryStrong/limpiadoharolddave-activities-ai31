<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CategoriesController;
use App\Http\Controllers\API\BookController;
use App\Http\Controllers\API\PatronController;
use App\Http\Controllers\API\BookBorrowedController;
use App\Http\Controllers\API\BookReturnedController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('books', BookController::class);
Route::apiResource('categories', CategoriesController::class);
Route::apiResource('patrons', PatronController::class);
Route::apiResource('borrowed_books', BookBorrowedController::class);
Route::apiResource('returned_books', BookReturnedController::class);

