<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\categories;
use App\Models\books;
use App\Models\patrons;
use App\Models\borrowed_books;

class BookBorrowedControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    
    public function  test_creating_an_api_book_borrowed_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];
        $category = categories::create($categoryData);

        $bookData = [
            'name' => 'The Notebook',
            'author' =>  $this->faker->name,
            'copies' =>  5,
            'category_id' => $category->id
        ];
        $book = books::create($bookData);

        $patronData = [
            'last_name' => $this->faker->lastName,
            'first_name' =>  $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'email' =>  $this->faker->email
        ];
        $patron = patrons::create($patronData);

        $borrowedData =[
            'patron_id' => $patron->id,
            'copies' => 5,
            'book_id' => $book->id
        ];

        $this->json('post', 'api/categories', $categoryData);
        $this->json('post', 'api/books', $bookData);
        $this->json('post', 'api/patrons', $patronData);
        
        $this->json('post', 'api/borrowed_books', $borrowedData)
             ->assertStatus(201);
    }

    public function  test_deleting_an_api_book_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];
        $category = categories::create($categoryData);

        $bookData = [
            'name' => 'The Notebook',
            'author' =>  $this->faker->name,
            'copies' =>  5,
            'category_id' => $category->id
        ];
        $book = books::create($bookData);

        $patronData = [
            'last_name' => $this->faker->lastName,
            'first_name' =>  $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'email' =>  $this->faker->email
        ];
        $patron = patrons::create($patronData);

        $borrowedData =[
            'patron_id' => $patron->id,
            'copies' => 5,
            'book_id' => $book->id
        ];

        $this->json('post', 'api/categories', $categoryData);
        $this->json('post', 'api/books', $bookData);
        $this->json('post', 'api/patrons', $patronData);

        $borrowed = borrowed_books::create($borrowedData);

        $this->json('delete', "/api/borrowed_books/$borrowed->id");
        $this->assertDatabaseMissing('borrowed_books', $borrowedData);
    }

    public function  test_updating_an_api_book_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];
        $category = categories::create($categoryData);

        $bookData = [
            'name' => 'The Notebook',
            'author' =>  $this->faker->name,
            'copies' =>  5,
            'category_id' => $category->id
        ];
        $book = books::create($bookData);

        $patronData = [
            'last_name' => $this->faker->lastName,
            'first_name' =>  $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'email' =>  $this->faker->email
        ];
        $patron = patrons::create($patronData);

        $borrowedData =[
            'patron_id' => $patron->id,
            'copies' => 5,
            'book_id' => $book->id
        ];

        $borrowed = borrowed_books::create($borrowedData);
        sleep(10);

        $this->json('post', 'api/categories', $categoryData);
        $this->json('post', 'api/books', $bookData);
        $this->json('post', 'api/patrons', $patronData);

        $this->json('put', "/api/borrowed_books/$borrowed->id", $borrowedData)
             ->assertStatus(200);
    }

    public function  test_showing_an_api_book_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];
        $category = categories::create($categoryData);

        $bookData = [
            'name' => 'The Notebook',
            'author' =>  $this->faker->name,
            'copies' =>  5,
            'category_id' => $category->id
        ];
        $book = books::create($bookData);

        $patronData = [
            'last_name' => $this->faker->lastName,
            'first_name' =>  $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'email' =>  $this->faker->email
        ];
        $patron = patrons::create($patronData);

        $borrowedData =[
            'patron_id' => $patron->id,
            'copies' => 5,
            'book_id' => $book->id
        ];

        $this->json('get', 'api/borrowed_books', $borrowedData)
             ->assertStatus(200);
    }
}
