<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\categories;
use App\Models\books;

class BookControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    
    public function  test_creating_an_api_book_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];

        $category = categories::create($categoryData);

        $bookData = [
            'name' => 'The Notebook',
            'author' =>  $this->faker->name,
            'copies' =>  5,
            'category_id' => $category->id
        ];

        $this->json('post', 'api/categories', $categoryData);
        
        $this->json('post', 'api/books', $bookData)
             ->assertStatus(201);
    }

    public function  test_deleting_an_api_book_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];

        $category = categories::create($categoryData);

        $bookData = [
            'name' => 'The Notebook',
            'author' =>  $this->faker->name,
            'copies' =>  5,
            'category_id' => $category->id
        ];

        $book = books::create($bookData);

        $this->json('delete', "/api/books/$book->id");
        $this->assertDatabaseMissing('books', $bookData);
    }

    public function  test_updating_an_api_book_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];

        $category = categories::create($categoryData);

        $bookData = [
            'name' => 'The Notebook',
            'author' =>  $this->faker->name,
            'copies' =>  5,
            'category_id' => $category->id
        ];

        $book = books::create($bookData);
        sleep(10);
        $this->json('post', 'api/categories', $categoryData);
        $this->json('put', "/api/books/$book->id", $bookData)
             ->assertStatus(200);
    }

    public function  test_showing_an_api_book_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];

        $category = categories::create($categoryData);

        $bookData = [
            'name' => 'The Notebook',
            'author' =>  $this->faker->name,
            'copies' =>  5,
            'category_id' => $category->id
        ];

        $this->json('get', 'api/books', $bookData)
             ->assertStatus(200);
    }
}
