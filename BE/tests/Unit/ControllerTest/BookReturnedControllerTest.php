<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\categories;
use App\Models\books;
use App\Models\patrons;
use App\Models\returned_books;

class BookReturnedControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    
    public function  test_creating_an_api_book_borrowed_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];
        $category = categories::create($categoryData);

        $bookData = [
            'name' => 'The Notebook',
            'author' =>  $this->faker->name,
            'copies' =>  5,
            'category_id' => $category->id
        ];
        $book = books::create($bookData);

        $patronData = [
            'last_name' => $this->faker->lastName,
            'first_name' =>  $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'email' =>  $this->faker->email
        ];
        $patron = patrons::create($patronData);

        $returnedData =[
            'book_id' => $book->id,
            'copies' => 5,
            'patron_id' => $patron->id,
            
        ];

        $this->json('post', 'api/categories', $categoryData);
        $this->json('post', 'api/books', $bookData);
        $this->json('post', 'api/patrons', $patronData);
        
        $this->json('post', 'api/returned_books', $returnedData)
             ->assertStatus(201);
    }

    public function  test_deleting_an_api_book_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];
        $category = categories::create($categoryData);

        $bookData = [
            'name' => 'The Notebook',
            'author' =>  $this->faker->name,
            'copies' =>  5,
            'category_id' => $category->id
        ];
        $book = books::create($bookData);

        $patronData = [
            'last_name' => $this->faker->lastName,
            'first_name' =>  $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'email' =>  $this->faker->email
        ];
        $patron = patrons::create($patronData);

        $returnedData =[
            'book_id' => $book->id,
            'copies' => 5,
            'patron_id' => $patron->id,
        ];

        $this->json('post', 'api/categories', $categoryData);
        $this->json('post', 'api/books', $bookData);
        $this->json('post', 'api/patrons', $patronData);

        $returned = returned_books::create($returnedData);

        $this->json('delete', "/api/returned_books/$returned->id");
        $this->assertDatabaseMissing('returned_books', $returnedData);
    }

    public function  test_updating_an_api_book_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];
        $category = categories::create($categoryData);

        $bookData = [
            'name' => 'The Notebook',
            'author' =>  $this->faker->name,
            'copies' =>  5,
            'category_id' => $category->id
        ];
        $book = books::create($bookData);

        $patronData = [
            'last_name' => $this->faker->lastName,
            'first_name' =>  $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'email' =>  $this->faker->email
        ];
        $patron = patrons::create($patronData);

        $returnedData =[
            'book_id' => $book->id,
            'copies' => 5,
            'patron_id' => $patron->id,
        ];

        $returned = returned_books::create($returnedData);
        sleep(10);

        $this->json('post', 'api/categories', $categoryData);
        $this->json('post', 'api/books', $bookData);
        $this->json('post', 'api/patrons', $patronData);

        $this->json('put', "/api/returned_books/$returned->id", $returnedData)
             ->assertStatus(200);
    }

    public function  test_showing_an_api_book_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];
        $category = categories::create($categoryData);

        $bookData = [
            'name' => 'The Notebook',
            'author' =>  $this->faker->name,
            'copies' =>  5,
            'category_id' => $category->id
        ];
        $book = books::create($bookData);

        $patronData = [
            'last_name' => $this->faker->lastName,
            'first_name' =>  $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'email' =>  $this->faker->email
        ];
        $patron = patrons::create($patronData);

        $returnedData =[
            'book_id' => $book->id,
            'copies' => 5,
            'patron_id' => $patron->id,
        ];

        $this->json('get', 'api/returned_books', $returnedData)
             ->assertStatus(200);
    }
}
