<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\patrons;
use App\Models\categories;
use App\Models\books;

class CategoryControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function  test_creating_an_api_category_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];

        $this->json('post', 'api/categories', $categoryData)
             ->assertStatus(201);
    }

    public function  test_deleting_an_api_category_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];

        $category = categories::create($categoryData);

        $this->json('delete', "/api/categories/$category->id");
        $this->assertDatabaseMissing('categories', $categoryData);
    }

    public function  test_updating_an_api_category_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];

        $category = categories::create($categoryData);

        sleep(10);

        $this->json('put', "/api/categories/$category->id", $categoryData)
             ->assertStatus(200);
    }

    public function  test_showing_an_api_category_request()
    {
        $categoryData = [
            'category' => 'Romance'
        ];

        $this->json('get', 'api/categories', $categoryData)
             ->assertStatus(200);
    }
}
