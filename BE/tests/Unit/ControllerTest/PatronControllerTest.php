<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\patrons;

class PatronControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;
    
    public function  test_creating_an_api_patron_request()
    {
        $patronData = [
            'last_name' => $this->faker->lastName,
            'first_name' =>  $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'email' =>  $this->faker->email
        ];

        $this->json('post', 'api/patrons', $patronData)
             ->assertStatus(201);
    }

    public function  test_deleting_an_api_patron_request()
    {
        $patronData = [
            'last_name' => $this->faker->lastName,
            'first_name' =>  $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'email' =>  $this->faker->email
        ];

        $patron = patrons::create($patronData);

        $this->json('delete', "/api/patrons/$patron->id");
        $this->assertDatabaseMissing('patrons', $patronData);
    }

    public function  test_updating_an_api_patron_request()
    {
        $patronData = [
            'last_name' => $this->faker->lastName,
            'first_name' =>  $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'email' =>  $this->faker->email
        ];

        $patron = patrons::create($patronData);

        sleep(10);

        $this->json('put', "/api/patrons/$patron->id", $patronData)
             ->assertStatus(200);
    }

    public function  test_showing_an_api_patron_request()
    {
        $patronData = [
            'last_name' => $this->faker->lastName,
            'first_name' =>  $this->faker->firstName,
            'middle_name' =>  $this->faker->lastName,
            'email' =>  $this->faker->email
        ];

        $this->json('get', 'api/patrons', $patronData)
             ->assertStatus(200);
    }
}
