<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class ModelTest extends TestCase
{

    /** @test */
    
    public function test_books_database_has_expected_columns()
    {
        $this->assertTrue( 
          Schema::hasColumns('books', [
            'id','name', 'author', 'copies', 'category_id'
        ]), 1);
    }

    public function test_categories_database_has_expected_columns()
    {
        $this->assertTrue( 
          Schema::hasColumns('categories', [
            'id','category'
        ]), 1);
    }

    public function test_borrowed_books_database_has_expected_columns()
    {
        $this->assertTrue( 
          Schema::hasColumns('borrowed_books', [
            'id','patron_id', 'copies', 'book_id'
        ]), 1);
    }

    public function test_returned_books_database_has_expected_columns()
    {
        $this->assertTrue( 
          Schema::hasColumns('returned_books', [
            'id','book_id', 'copies', 'patron_id'
        ]), 1);
    }

    public function test_patrons_database_has_expected_columns()
    {
        $this->assertTrue( 
          Schema::hasColumns('patrons', [
            'id','last_name', 'first_name', 'middle_name', 'email'
        ]), 1);
    }
    
}
