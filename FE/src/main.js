import Vue from 'vue'
import App from './App'
import router from './router'
import toastr from "vue-toastr";
import store from './store/index'

Vue.use(toastr,{
  defaultTimeout: 3000,
  defaultProgressBar: false,
  defaultProgressBarValue: 0,
  defaultType: "error",
  defaultPosition: "toast-bottom-left",
  defaultCloseOnHover: true,
  defaultStyle: { "background-color": "red" },
  })

//Bootstrap
Vue.use(BootstrapVue)
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'



//Navigations
import NavBar from './components/navigations/NavBar.vue'
Vue.component('navigation', NavBar)

import SideBar from './components/navigations/SideBar.vue'
Vue.component('sidebar', SideBar)

//CSS
require('./assets/css/style.css')

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
