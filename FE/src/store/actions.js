import axios from "@/assets/js/axiosConfig";

//patron
export const fetchPatrons = ({commit}) => {
    axios.get('/patrons')
        .then(response =>{
            commit('setPatrons', response.data);
        })
}

export const createPatrons = ({ commit }, {first_name, middle_name, last_name, email}) => {
    axios.post('/patrons', {first_name, middle_name, last_name, email})
        .then(response =>{
       commit('createPatron',  response.data);
        })
}

export const deletePatrons = ({ commit }, patron) => {
    axios.delete(`/patrons/${patron.id}`)
        .then(response =>{
       commit('removePatrons',  response.data);
        })
}

//categories
export const fetchCategories = ({commit}) => {
    axios.get('/categories')
        .then(response =>{
            commit('setCategories', response.data);
        })
}

//books
export const fetchBooks = ({commit}) => {
    axios.get('/books')
        .then(response =>{
            commit('setBooks', response.data);
        })
}

export const createBooks = ({ commit }, {name, author , copies, category_id}) => {
    axios.post('/books', {name, author, copies, category_id})
        .then(response =>{
       commit('createBook',  response.data);
        })
}