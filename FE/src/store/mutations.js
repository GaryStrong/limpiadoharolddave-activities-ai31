//patron
export const setPatrons = (state, patrons) =>{
    state.patrons = patrons
}

export const createPatron = (state, {first_name, middle_name, last_name, email}) =>{
    state.patrons.push({
        first_name,
        middle_name,
        last_name,
        email
    })
}

export const removePatrons = (state, patrons) =>{
    state.patrons = state.patrons.filter(item => {
        return item.patrons.id !== patrons.id;
    })
}

//books
export const setBooks = (state, books) =>{
    state.books = books
}

export const createBook = (state, {name, author, copies, category_id}) =>{
    state.books.push({
        name,
        author,
        copies,
        category_id
    })
}

//categories
export const setCategories = (state, categories) =>{
    state.categories = categories
}