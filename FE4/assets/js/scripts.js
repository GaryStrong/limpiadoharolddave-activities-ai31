var ctx = document.getElementById('myChartBar').getContext('2d');
var chart = new Chart(ctx, {


    type: 'bar',

    data: {
        labels: ['March', 'April', 'May', 'June', 'July'],
        datasets: [{
            label: 'Number of books',
            backgroundColor: 'rgb(18, 35, 46)',
            borderColor: 'rgb(18, 35, 46)',
            data: [280, 500, 600, 350, 180]
        }]
    },

    options: {
        title:{
            display: true,
            position: "top",
            text: "B O R R O W E D   A N D   R E T U R N E D   B O O K S",
            fontSize: 18,
            fontColor: "#12232E"
        },
        legend:{
            display: false
        },
        scales:{
            xAxes:[{
                gridLines:{
                    display:false,
                    stacked: true
                }
            }],
            yAxes:[{
                ticks:{
                    beginAtZero: true
                }
            }]
        }
    }
});


var ctxs = document.getElementById('myChartPie').getContext('2d');
var chart = new Chart(ctxs, {


    type: 'doughnut',

    data: {
        labels: ['HowToGraduate'],
        datasets: [{
            label: 'Number of books',
            backgroundColor: "#1D7DC3",
            borderColor: "#1D7DC3",
            data: [100]
        }]
    },

    options: {
        title:{
            display: true,
            position: "top",
            text: "P O P U L A R   B O O K   G E N R E",
            fontSize: 18,
            fontColor: "#12232E"
        },

        legend:{
            position: 'bottom',

            labels:{
                fontFamily: 'Nunito',
                fontStyle: 'bold'
            }
        }
    }
});

//FOR MODAL
var ctx = document.getElementById('myChartBar2').getContext('2d');
var chart = new Chart(ctx, {


    type: 'bar',

    data: {
        labels: ['March', 'April', 'May', 'June', 'July'],
        datasets: [{
            label: 'Number of books',
            backgroundColor: 'rgb(18, 35, 46)',
            borderColor: 'rgb(18, 35, 46)',
            data: [280, 500, 600, 350, 180]
        }]
    },

    options: {
        title:{
            display: true,
            position: "top",
            text: "B O R R O W E D   A N D   R E T U R N E D   B O O K S",
            fontSize: 14,
            fontColor: "#12232E"
        },
        legend:{
            display: false
        },
        scales:{
            xAxes:[{
                gridLines:{
                    display:false,
                    stacked: true
                }
            }],
            yAxes:[{
                ticks:{
                    beginAtZero: true
                }
            }]
        }
    }
});
//FOR MODAL
var ctxs = document.getElementById('myChartPie2').getContext('2d');
var chart = new Chart(ctxs, {


    type: 'doughnut',

    data: {
        labels: ['HowToGraduate'],
        datasets: [{
            label: 'Number of books',
            backgroundColor: "#1D7DC3",
            borderColor: "#1D7DC3",
            data: [100]
        }]
    },

    options: {
        title:{
            display: true,
            position: "top",
            text: "P O P U L A R   B O O K   G E N R E",
            fontSize: 18,
            fontColor: "#12232E"
        },

        legend:{
            position: 'bottom',

            labels:{
                fontFamily: 'Nunito',
                fontStyle: 'bold'
            }
        }
    }
});